<?php 

global $post;

get_header(); 

if(have_posts()) :
	while(have_posts()) :
		the_post();
	?>
	<div class="sections-wrapper">
		<div class="full section dark-grey terms-conditions single-page">
		<div class="container">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
	<?php
	endwhile;
endif;

wp_reset_postdata();

?>

<?php get_footer(); ?>