<?php
	//enqueue the skeleton framework
	
	add_action('wp_enqueue_scripts', 'enqueue_sripts');
	
	function enqueue_sripts(){
		global $post;
		wp_register_script( 'jquery_appear', get_template_directory_uri() . '/scripts/jquery.appear.js', array('jquery'), 1.0, false );
		wp_register_script( 'lazy_load', '//cdnjs.cloudflare.com/ajax/libs/jquery.lazyloadxt/1.0.0/jquery.lazyloadxt.min.js', array('jquery'), 1.0, false );
		wp_register_script( 'lazy_load_background', get_template_directory_uri() . '/scripts/jquery.lazyloadxt.bg.min.js', array('lazy_load'), 1.0, false );
		wp_register_script( 'lazy_load_orig', get_template_directory_uri() . '/scripts/jquery.lazyload.js', array('jquery'));

		//register the fullpage.js script; we also grab the colors array from the database
		wp_register_script('fullpage', get_template_directory_uri() . '/scripts/fullpage/jquery.fullPage.js', $scripts_array);
		

		//localize an array of slide colors for correct fullpage.js functionality
		$colors_array = array('#FFFFFF', '#535353');
		$args = array(
			'post_type'   => 'portfolio-item',
			'post_status' => 'publish',
			'posts_per_page'	=>	-1,
			// //Custom Field Parameters
			'meta_key'       => '_featured-peice',
			'meta_value'     => 'true',
			'orderby'			=> 'menu_order',
			'order'				=> 'ASC'
		);

		$query = new WP_Query( $args );
		// The Loop
		if ( $query->have_posts() ) :
			while ( $query->have_posts() ) :
				$query->the_post();
				array_push($colors_array, get_post_meta($post->ID, '_homepage-color', true));
			endwhile;
		endif;

		wp_reset_postdata();

		wp_localize_script('fullpage', 'colors_array', $colors_array);
		wp_enqueue_script('fullpage');


		wp_register_script( 'hoverIntent', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.hoverintent/1.8.1/jquery.hoverIntent.min.js', array('jquery'), 1.0, false );
		$scripts_array = array('jquery', 'jquery-effects-core', 'hoverIntent', 'jquery_appear', 'lazy_load_background');
		/*if(!isMobile()) {
			array_push($scripts_array, 'adobe_edge');
		}*/
		wp_enqueue_script('pica_script', get_template_directory_uri() . '/scripts/scripts.js', $scripts_array, 1.0, false);
		wp_enqueue_script('lazy_load_orig');
	}	

	function inject_tkf() {	//POST: injecting the typekit fonts into the header.
		echo '<script> var ajaxUrl = "' . admin_url('admin-ajax.php') . '"</script>';
		echo '<script src="//use.typekit.net/hhf5nor.js"></script>';
		echo '<script>try{Typekit.load();}catch(e){}</script>';
	}
	add_action('wp_head', 'inject_tkf', 1);