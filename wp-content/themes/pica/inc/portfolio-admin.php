<?php 
	add_filter( 'admin_post_thumbnail_html', 'add_featured_image_instruction');
	function add_featured_image_instruction( $content ) {
		global $post;
		//this is to get make sure this is only available on the portfolio peices

		if($post->post_type == 'portfolio-item'): // if portfolio page
			//render the html and add in a select box to choose which side the image goes on

	    	return $content .= '<h4>Homepage Image Placement: </h4>
					<input type="radio" name="image-position" value="left" ' . checked('left', get_post_meta($post->ID, '_image-position', true), false) . '> Left<br />
					<input type="radio" name="image-position" value="right" ' . checked('right', get_post_meta($post->ID, '_image-position', true), false) . '> Right<br />';
		endif;	//endif;
	}