<?php

/**
 * Container [container]content[/container]
 * Props: none
 * outputs: .containter div with content from admin inside
 */
function container_shortcode( $atts, $content = null ) {
	global $post;
	return '<div class="container ' . $post->post_name . ' ' . $atts['vertical'] . '" style="height: '.$atts['height'].'px; color:'.$atts['font_color'].'; text-align:'.$atts['align'].';">' . do_shortcode(str_replace('<br />', '', $content)) . '</div>';
}
add_shortcode( 'container', 'container_shortcode' );

/**
 * Container with background [container_background]content[/container_background]
 * shortcode: container_background
 * properties: 
 *		color: use a supported css color formatq
 *		img_url: url of the background image to be used.
 */
function container_with_background($atts, $content = null){
	global $post;
	if($atts['img_url']) $lazyload = "lazy";
	return '<div class="container ' . $post->post_name . ' ' . $atts['margin'] . ' ' . $lazyload . '" data-original="' . $atts['img_url'] . '" style="background-color:' . $atts['color'] . '; height: '.$atts['height'].'px; color:'.$atts['font_color'].'; background-size: ' . $atts['max_height'] . 'px auto;">' . do_shortcode(str_replace('<br />', '', $content)) . '</div>';
}
add_shortcode('container_background', 'container_with_background');
/**
 * Half Column [half_column][/half_column]
 shortcode: half_column
 properties:
 	position: left/right
 	img_url: url for background
 	color: background color in support css color
 	font-color: font color in supported css color
 	height: int example 250 in pixels
 */
function half_column($atts, $content=null){
	if($atts['img_url']) $lazyload = "lazy";
	return '<div class="half col ' . $atts['position'] . ' ' . $atts['margin'] . ' ' . $atts['text'] . ' ' . $lazyload . '" data-original="' . $atts['img_url'] . '" style="background-color:' . $atts['color'] . '; color:' . $atts['font_color'] . '; height:'.$atts['height'].'px;"><div class="inner">' . $content . '</div></div>';
}
add_shortcode('half_column', 'half_column');

function one_third_column($atts, $content=null){
	if($atts['img_url']) $lazyload = "lazy";
	return '<div class="one-third col ' . $atts['position'] . ' ' . $atts['margin'] . ' ' . $lazyload . '" data-original="' . $atts['img_url'] . '" style="background-color:' . $atts['color'] . '; color:' . $atts['font-color'] . '; height:'.$atts['height'].'px"><div class="inner">' . $content . '</div></div>';
}
add_shortcode('one_third', 'one_third_column');

function two_third_column($atts, $content=null){
	if($atts['img_url']) $lazyload = "lazy";
	return '<div class="two-third col ' . $atts['position'] . ' ' . $lazyload . '" data-original="' . $atts['img_url'] . '" style="background-color:' . $atts['color'] . '; color:' . $atts['font-color'] . '; height:'.$atts['height'].'px">' . $content . '</div>';
}
add_shortcode('two_third', 'two_third_column');