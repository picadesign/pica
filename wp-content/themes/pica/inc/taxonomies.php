<?php 
	$labels = array(
		'name' => __('Project Types'),
		'singular_name' => __('Project Type'),
		'all_items'	=> __('All Project Types'),
		'edit_item' => __('Edit Project Type'),
		'view_item' => __('View Project Type'),
		'update_item' => __('Update Project Type'),
		'add_new_item' => __('Add New Project Type'),
		'new_item_name' => __('New Project Type'),
		'parent_item' => __('Parent Project Type'),
		'parent_item_colon' => __('Parent Project Type:'),
		'search_items' => __('Search Project Types'),
		'popular_items' => __('Popular Project Types'),
		'separate_items_with_commas' => __('Separate project types with commas'),
		'add_or_remove_items' => __('Add or remove project types'),
		'choose_from_most_used' => __('Choose from the most used projec types'),
		'not_found' => __('No project types found')
	);
	$args = array(
		'labels' => $labels,
		'hierarchical'      => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'project-type' ),
		'public'			=> true
	);
	register_taxonomy( 'project-type', 'portfolio-item', $args );