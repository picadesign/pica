<?php

/**
* Registers a new portfolio post type
* @uses $wp_post_types Inserts new post type object into the list
*
* @param string  Post type key, must not exceed 20 characters
* @param array|string  See optional args description above.
* @return object|WP_Error the registered post type object, or an error object
*/

$labels = array(
	'name'                => __( 'Portfolio Items', 'pica' ),
	'singular_name'       => __( 'Portfolio Item', 'pica' ),
	'add_new'             => _x( 'Add New Portfolio Item', 'pica', 'pica' ),
	'add_new_item'        => __( 'Add New Portfolio Item', 'pica' ),
	'edit_item'           => __( 'Edit Portfolio Item', 'pica' ),
	'new_item'            => __( 'New Portfolio Item', 'pica' ),
	'view_item'           => __( 'View Portfolio Item', 'pica' ),
	'search_items'        => __( 'Search Portfolio Items', 'pica' ),
	'not_found'           => __( 'No Portfolio Items found', 'pica' ),
	'not_found_in_trash'  => __( 'No Portfolio Items found in Trash', 'pica' ),
	'parent_item_colon'   => __( 'Parent Portfolio Item:', 'pica' ),
	'menu_name'           => __( 'Portfolio Items', 'pica' ),
);

$args = array(
	'labels'                   => $labels,
	'hierarchical'        => false,
	'description'         => 'The post type used for Pica portfolio peices.',
	'taxonomies'          => array('project-type'),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	//'menu_position'       => null,
	'menu_icon'           => 'dashicons-images-alt',
	'show_in_nav_menus'   => true,
	'publicly_queryable'  => true,
	//'exclude_from_search' => false,
	//'has_archive'         => true,
	//'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => array('slug' => 'portfolio-item'),
	'capability_type'     => 'post',
	'supports'            => 
	array(
		'title', 
		'editor', 
		'thumbnail',
		'revisions',
		'excerpt'
	)
);

register_post_type( 'portfolio-item', $args );

//Meta Boxes
function add_portfolio_meta_boxes() {
	//color metabox
	add_meta_box(
		'homepage-background-color',
		__( 'Homepage Background Color'),
		'color_contents',
		'portfolio-item',
		'side'
	);
	add_meta_box(
		'featured-piece',
		__('Feature Portfolio Piece'),
		'feature_peice_box_contents',
		'portfolio-item',
		'side'
	);
	add_meta_box(
		'testimonial',
		__('Client Testimonial'),
		'testimonials_contents',
		'portfolio-item'
	);
}
add_action( 'add_meta_boxes', 'add_portfolio_meta_boxes' );

//render color meta box contents
function color_contents( $post ) {
	$value = get_post_meta( $post->ID, '_homepage-color', true );
	echo '<input type="color" id="homepage-color" name="homepage-color" value="' .  $value  . '"/>';
}

function feature_peice_box_contents($post){
	$value = get_post_meta( $post->ID, '_featured-peice', true);
	echo '<h4>Feature this on the hompage? </h4>';
	echo '<input type="radio" name="featured-piece" value="true" ' . checked('true', get_post_meta($post->ID, '_featured-peice', true), false) . '> Yes <br>';
	echo '<input type="radio" name="featured-piece" value="false" ' . checked('false', get_post_meta($post->ID, '_featured-peice', true), false) . '> No';
}

function testimonials_contents($post){
	$testimonial = get_post_meta($post->ID, '_testimonial', true); ?>
<textarea id="excerpt" name="testimonial-content"><?php echo $testimonial['content'] ?></textarea>
	<h4>Client Name</h4>
	<input type="textarea" name="testimonial-name" value="<?php echo $testimonial['name'] ?>">
	<h4>Client Credentials</h4>
	<input type="textarea" name="testimonial-creds" value="<?php echo $testimonial['creds'] ?>">
<?php }

//fires when the portfolio item post gets saved.
function save_portfolio_meta( $post_id, $post, $update ) {
    $slug = 'portfolio-item';

    // If this isn't a 'portfolio' post, don't update it.
    if ( $slug != $post->post_type ) {
        return;
    }
   //print_r($_POST);
    $testimonial = array(
    	'content' => $_POST['testimonial-content'],
    	'name' => $_POST['testimonial-name'],
    	'creds' => $_POST['testimonial-creds']
    );
    update_post_meta($post_id, '_testimonial', $testimonial);
    update_post_meta($post_id, '_homepage-color', $_POST['homepage-color']);
    update_post_meta( $post_id, '_featured-peice', $_POST['featured-piece']);
    update_post_meta($post_id, '_image-position', $_POST['image-position']);
    //print_r($_REQUEST['image-position']);
}
add_action( 'save_post', 'save_portfolio_meta', 10, 3 );