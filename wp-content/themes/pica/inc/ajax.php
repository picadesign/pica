<?php

	function submit_contact_form(){
		$to = get_bloginfo('admin_email');
		$subject = 'Contact Form Submitted';
		$message = $_POST['message'];
		// assumes $to, $subject, $message have already been defined earlier...

		$headers[] = 'From: '.$_POST['name'].' <'.$_POST['email'].'>';

		$status = wp_mail( $to, $subject, $message, $headers );
		echo json_encode($status);
		die();
	}
	add_action('wp_ajax_submit_contact_form', 'submit_contact_form');
	add_action('wp_ajax_nopriv_submit_contact_form', 'submit_contact_form');


	function get_portfolio_content(){
		global $post;
		$post = get_post((int)$_POST['post_id']);
		setup_postdata($post);
		remove_filter('the_content', 'wpautop');
		the_content();
		add_filter('the_content', 'wpautop');
		die();
	}
	add_action('wp_ajax_get_portfolio_content', 'get_portfolio_content');
	add_action('wp_ajax_nopriv_get_portfolio_content', 'get_portfolio_content');