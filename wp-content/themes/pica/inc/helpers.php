<?php

function get_avtr_url($get_avatar) {
	preg_match('/src="(.*?)"/i', $get_avatar, $matches);
	return $matches[1];
}

function isMobile() {
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}

?>