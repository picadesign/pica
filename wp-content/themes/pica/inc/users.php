<?php
	//add extra fields for the user meta
	add_action( 'show_user_profile', 'display_user_meta' );
	add_action( 'edit_user_profile', 'display_user_meta' );
	function display_user_meta( $user ) { ?>
	    <h3>Extra Bits</h3>
	    <table class="form-table">
	    	<tr>
	            <th><label>Position Title</label></th>
	            <td><input type="text" name="position_title" value="<?php echo get_user_meta( $user->ID, '_position_title', true ); ?>" class="regular-text" /></td>
	        </tr>
	        <tr>
	            <th><label>Home Page Menu Order</label></th>
	            <td><input type="number" name="menu_order" value="<?php echo get_user_meta( $user->ID, '_user_menu_order', true ); ?>" class="regular-text" /></td>
	        </tr>
	    </table>
	    <h3>Social Links</h3>
	    <table class="form-table">
	    	<tr>
	            <th><label>Facebook</label></th>
	            <td><input type="text" name="facebook" value="<?php echo get_user_meta( $user->ID, '_user_facebook', true ); ?>" class="regular-text" /></td>
	        </tr>
	        <tr>
	            <th><label>Twitter</label></th>
	            <td><input type="text" name="twitter" value="<?php echo get_user_meta( $user->ID, '_user_twitter', true ); ?>" class="regular-text" /></td>
	        </tr>
	        <tr>
	            <th><label>Instagram</label></th>
	            <td><input type="text" name="instagram" value="<?php echo get_user_meta( $user->ID, '_user_instagram', true ); ?>" class="regular-text" /></td>
	        </tr>
	        <tr>
	            <th><label>Linkedin</label></th>
	            <td><input type="text" name="linkedin" value="<?php echo get_user_meta( $user->ID, '_user_linkedin', true ); ?>" class="regular-text" /></td>
	        </tr>
	    </table>
	    <?php
	}

	//update the user with the meta
	add_action('edit_user_profile_update', 'update_extra_profile_fields');
	function update_extra_profile_fields($user_id) {
	 if ( current_user_can('edit_user',$user_id) )
		update_user_meta($user_id, '_user_menu_order', $_POST['menu_order']);
		update_user_meta($user_id, '_position_title', $_POST['position_title']);
		update_user_meta($user_id, '_user_facebook', $_POST['facebook']);
		update_user_meta($user_id, '_user_twitter', $_POST['twitter']);
		update_user_meta($user_id, '_user_instagram', $_POST['instagram']);
		update_user_meta($user_id, '_user_linkedin', $_POST['linkedin']);
	}