<?php 
	//enqueue the skeleton framework
add_action('wp_enqueue_scripts', 'enqueue_styles');
function enqueue_styles(){
	wp_enqueue_style( 'normalize', get_bloginfo('template_directory'). '/stylesheets/skeleton/normalize.css', false, '0.1', 'all' );
	wp_enqueue_style( 'skeleton', get_bloginfo('template_directory'). '/stylesheets/skeleton/skeleton.css', false, '0.1', 'all' );
	
	//enqueue the main site stylesheet
	wp_enqueue_style( 'pica_style', get_bloginfo('template_directory'). '/stylesheets/style.css', false, '0.1', 'all' );
}