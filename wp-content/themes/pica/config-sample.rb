#Project Settings
environment = :development
project_type = :stand_alone
preferred_syntax = :scss
output_style = :nested #:nested, :expanded, :compact, or :compressed.
line_comments = true

#Asset Paths
sass_dir = "sass"
css_dir = "stylesheets"
http_images_path = "//dev.pica.is/new_pica/wp-content/themes/pica/images/"
http_generated_images_path = "//dev.pica.is/new_pica/wp-content/themes/pica/images/"
generated_images_path = "/Applications/MAMP/htdocs/new_pica/wp-content/themes/pica/images/"
http_fonts_path = "//dev.pica.is/new_pica/wp-content/themes/pica/fonts/"
sprite_load_path = "/Applications/MAMP/htdocs/new_pica/wp-content/themes/pica/images/"