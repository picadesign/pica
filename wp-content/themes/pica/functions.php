<?php

	// Pica
	// Branding, Marketing + Design

	//Theme Setup

	add_action('init', 'pica_theme_setup');
	function pica_theme_setup(){	//POST: the main function for the theme setup
		//include the files in here for the
		
		//Include the styles
		include('inc/styles.php');
		include('inc/scripts.php');

		//Add the custom post types.
		include('inc/post-types.php');

		//add the ajax actions
		include('inc/ajax.php');

		//Add the custom taxonomies.
		include('inc/taxonomies.php');

		//Add the post thumbnail supports
		include('inc/post-thumbnails.php');

		//Add the helper functions
		include('inc/helpers.php');

		//add the shortcode file
		include('inc/shortcodes.php');
		remove_filter('the_content', 'wpautop');
		add_filter('the_content', 'wpautop');
	}

	

	add_action('admin_init', 'pica_admin_theme_setup');
	function pica_admin_theme_setup(){ //POST: the main function for the admin functions
		//portfolio admin
		include('inc/portfolio-admin.php');

		//functions for user meta and other various bits
		include('inc/users.php');
	}