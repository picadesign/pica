/*
 *	SnapPoint jQuery Plugin
 *	Version 2.05
 *	Author: Robert Spangler (http://robspangler.com/)
 *
 *	Copyright (c) 2012
 *	Licensed under the MIT license.
 *
 *
 *	Left-to-Right snap added by
 *
 *	Ken Frederick
 *	ken.frederick@gmx.de
 *
 *	http://cargocollective.com/kenfrederick/
 *	http://kenfrederick.blogspot.com/
 *
 */
(function(b){jQuery.browser={};jQuery.browser.mozilla=/mozilla/.test(navigator.userAgent.toLowerCase())&&!/webkit/.test(navigator.userAgent.toLowerCase());jQuery.browser.webkit=/webkit/.test(navigator.userAgent.toLowerCase());jQuery.browser.opera=/opera/.test(navigator.userAgent.toLowerCase());jQuery.browser.msie=/msie/.test(navigator.userAgent.toLowerCase());var a=null;b.fn.snapPoint=function(c){var e={scrollDelay:550,scrollSpeed:90,outerTopOffset:200,innerTopOffset:0,outerLeftOffset:200,innerLeftOffset:0};c=b.extend({},e,c);elArray=this;b(window).scroll(function(){if(a){clearTimeout(a)}a=setTimeout(d,c.scrollDelay)});a=null;function d(){elArray.each(function(){var f=b(this).position();elementPosTop=f.top;elementPosLeft=f.left;windowPosTop=b(jQuery.browser.webkit?"body":"html").scrollTop();if(windowPosTop+c.outerTopOffset>=elementPosTop&&elementPosTop+c.innerTopOffset>=windowPosTop&&windowPosTop!=elementPosTop){b(jQuery.browser.webkit?"body":"html").animate({scrollTop:elementPosTop},c.scrollSpeed)}windowPosLeft=b(jQuery.browser.webkit?"body":"html").scrollLeft();if(windowPosLeft+c.outerLeftOffset>=elementPosLeft&&elementPosLeft+c.innerLeftOffset>=windowPosLeft&&windowPosLeft!=elementPosLeft){b(jQuery.browser.webkit?"body":"html").animate({scrollLeft:elementPosLeft},c.scrollSpeed)}})}}})(jQuery);