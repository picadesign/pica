jQuery(document).ready(function($){

	var fullpage = false;
	var pathname = window.location.pathname;
	function fullpage_initialize() {
		if(pathname == "/") {
			fullpage = true;
			$('#fullpage').fullpage({
				menu: false,
				lockAnchors: false,
				navigation: true,
				navigationPosition: 'left',
				showActiveTooltip: false,
				slidesNavigation: false,
				css3: true,
				scrollingSpeed: 700,
				autoScrolling: true,
				fitToSection: false,
				scrollBar: true,
				easing: 'easeInOutCubic',
				easingcss3: 'ease',
				loopBottom: false,
				loopTop: false,
				loopHorizontal: false,
				continuousVertical: false,
				scrollOverflow: false,
				touchSensitivity: 5,
				normalScrollElementTouchThreshold: 10,
				keyboardScrolling: true,
				animateAnchor: true,
				recordHistory: true,
				controlArrows: false,
				verticalCentered: false,
				sectionSelector: '.section',
				slideSelector: '.slide',
				responsiveWidth: 768,
				responsiveHeight: 0,
				sectionsColor: colors_array,
				onLeave: function(index, nextIndex, direction) {

				},
				afterLoad: function(anchorLink, index) {

				},
				afterRender: function() {

				},
				afterResize: function() {

				},
				afterSlideLoad: function(anchorLink, index, slideAnchor, slideIndex) {

				},
				onSlideLeave: function(anchorLink, index, slideIndex, direction, nextSlideIndex) {

				}
			});
		}
	}

	// jQuery(".section").snapPoint({
	//     scrollDelay: 50,       // Amount of time the visitor has to scroll before the snap point kicks in (ms)
	//     scrollSpeed: 300,        // Length of smooth scroll's animation (ms)
	//     outerTopOffset: jQuery('.full').height()*.75,    // Number of pixels for the downward vertical offset (relative to the top of your snapping container)
	//     innerTopOffset: jQuery('.full').height()*.25,      // Number of pixels for the upward vertical offset (relative to the top of your snapping container)
	//     outerLeftOffset: 200,   // Number of pixels for the outer horizontal offset (relative to the right of your snapping container)
	//     innerLeftOffset: 0      // Number of pixels for the inner horizontal offset (relative to the left of your snapping container)

	// });
	//

	$(window).ready(function(){
		$('.hero').addClass('loaded');
		function animate_mobile() {
			$('#mobile_animation').animate({
				top: "-=5vh",
			}, { duration: 1000, queue: false});
			$('#mobile_banner').animate({
				opacity: 0.48
			}, { duration: 1000, queue: false});
			//$('#brand span').text('SMART');
			// $('#brand').css('opacity', 0).delay(1000).animate({
			// 	opacity: 1
			// }, 400, function() {
			// 	$('#brand span').delay(500).animate({
			// 		opacity: 0
			// 	}, 400, function() {
			// 		$('#brand span').text('SIMPLE').animate({
			// 			opacity: 1
			// 		}, 400, function() {
			// 			$('#brand span').delay(500).animate({
			// 				opacity: 0
			// 			}, 400, function() {
			// 				$('#brand span').text('STRONG.').animate({
			// 					opacity: 1
			// 				}, 400, function(){});
			// 			});
			// 		});
			// 	});
			// });
			$('#brand').delay(1000).animate({
				opacity: 1
			}, 300, function(){
				var self = $(this);
				self.children('span:first-child').delay(600).animate({
					opacity: 0
				}, 300, function(){
					self.children('span:nth-child(2)').animate({
						opacity:1
					}, 300, function(){
						$(this).delay(400).animate({
							opacity:0
						}, 300, function(){
							self.children('span:nth-child(3)').animate({
								opacity: 1
							},300, function(){});
						});
					});
				});
			});
		}
		setTimeout(animate_mobile, 1000);
	});

	if(/Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || $(window).width() < 768) {
		$('.people-overlay').css('height', '100%');
		$('.person-info').css('bottom', '5%');
		$('.col', function() {
			if($(this).height() == "550") {
				$(this).css('height', '400px');
			}
		});
	} else{
		$('.people-overlay').css('height', '0');
		$('.person-info').css('bottom', '-100px');
		if(fullpage === false) {
			fullpage_initialize();
		}
	}

	if(/iPad/i.test(navigator.userAgent)) {
		$('.people-overlay').css('height', '100%');
		$('.person-info').css('bottom', '5%');
	}


	$(window).resize(function(){
		if($(window).width() < 768 || /Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
			$('.people-overlay').css('height', '100%');
			$('.person-info').css('bottom', '5%');
		} else {
			if(fullpage === false) {
				fullpage_initialize();
			}
			$('.people-overlay').css('height', '0');
			$('.person-info').css('bottom', '-100px');
		}

		if(/iPad/i.test(navigator.userAgent)) {
			$('.people-overlay').css('height', '100%');
			$('.person-info').css('bottom', '5%');
		}
	});

	$('.down-arrow').click(function(){
		if($(this).parent().data('ga-shortname')) {
			ga('send', 'event', 'navigation', 'click', 'down-arrow ' + $(this).parent().data('ga-shortname'));
		} else {
			ga('send', 'event', 'navigation', 'click', 'down-arrow ' + $(this).parent().attr('class'));
		}
		$("html, body").animate({scrollTop: $($(this).parent().nextAll('.full')[0]).offset().top}, 750);
		return false;
	});

	//if there is a # anchor in the current page URL
	if(window.location.hash.indexOf('#') !== -1) {
		//replace it with a class name
		var elem = $('.' + window.location.hash.replace('#', ''));

		//if a DOM element exists with this class name
		if(elem) {

			//scroll to the element
			$('html, body').animate({scrollTop: $('.section' + elem.selector).offset().top}, 750);
		}
	}

	function close_portfolio() {
		$('.underlay, .portfolio-display').animate({
			opacity:0
		}, 350);
		$('html, body').animate({
			scrollTop: current_place
		}, 350);
		$('.underlay').toggle();
		$('.portfolio-display').css('display', 'none');
	}

	$('.underlay, .close').on('click', function(e, prev) {
		if($(window).width() > 900) {
			fullpage_initialize();
		}
		if(!prev) {
			ga('send', 'event', 'popups', 'click', 'close ' + $(this).parent().attr('class'));
		}
		$('html, body').css('overflow', default_overflow);
		close_portfolio();
	});

	$('#home').click(function(){
		$('html, body').animate({scrollTop: $('.home').offset().top}, 750);
		ga('send', 'event', 'navigation', 'click', 'nav-button home-logo');
	});
	$('#work').click(function(){
		$('html, body').animate({scrollTop: $('.portfolio-item').offset().top}, 750);
		ga('send', 'event', 'navigation', 'click', 'nav-button work');
	});
	$('#clients').click(function(){
		$('html, body').animate({scrollTop: $('.section.clients').offset().top}, 750);
		ga('send', 'event', 'navigation', 'click', 'nav-button clients');
	});
	$('#about').click(function(){
		$('html, body').animate({scrollTop: $('.profiles').offset().top}, 750);
		ga('send', 'event', 'navigation', 'click', 'nav-button about');
	});
	$('#contact').click(function(e, footer){
		$('html, body').animate({scrollTop: $('.contact').offset().top}, 750);
		ga('send', 'event', 'navigation', 'click', 'nav-button contact');
	});

	$(document).on('scroll', function(){
		if($('.portfolio-display').length) {
			var pdBottom = $(".portfolio-display").height();
			var winh = $(window).height() - $(".portfolio-display").offset().top - $(".portfolio-display").height();
		}
	});

	//capture Analytics event when user clicks any external/internal Anchor on the site
	$('a').click(function() {
		ga('send', 'event', 'anchors', 'click', $(this).attr('href'));
	});

	//when user clicks on view project
	var current_place, viewing_project;
	var default_overflow = $('html, body').css('overflow-y');
	$('.view-project').click(function(e, prev) {
		viewing_project = $(this).data('project-name');
		var proj_name = $(this).data('project-name');
		//only trigger a click event if the user didn't click a More Work button
		if(!prev) {
			ga('send', 'event', 'popups', 'click', 'view-work ' + $(this).data('project-name'));
			if($('.portfolio-display .' + proj_name).length === 0) {
				ga('send', 'event', 'error', 'view-work', 'preload-failure');
			}
		}
		$('.portfolio-display.' + proj_name).find('.lazy').trigger('lazy_trigger');
		$('.portfolio-display.' + proj_name).toggle();
		$('.underlay').toggle();

		//send an Analytics event for viewing the current project
		ga('send', 'event', 'popups', 'view-project', proj_name);
		current_place = $(this).parentsUntil('.portfolio-item').offset().top;
		//console.log(current_place);
		$('html, body, .underlay, .portfolio-display.' + proj_name).animate({
			scrollTop: 0,
			opacity: 1
		}, 650, function() {
			if($(window).width() > 900) {
				$.fn.fullpage.destroy('all');
			}
			$('html, body').css('overflow', 'hidden');
		});
	});

	var all_projects = [];
	$.each($('.view-project'), function() {
		all_projects.push($(this).data('project-name'));
	});
	var last_proj_elem = all_projects.length;

	$('.prev_next').on('click', '.more_work', function() {
		//send an Analytics event for clicking the more-work button, and the current project name
		ga('send', 'event', 'popups', 'click', 'more-work ' + viewing_project);
		var this_view = viewing_project;
		var this_element = all_projects.indexOf(this_view);
		var next_element = all_projects[this_element + 1];
		if(!next_element) next_element = all_projects[0];
		$('.close.' + this_view).trigger('click', true);
		$('.view-project[data-project-name="' + next_element + '"]').trigger('click', true);
	});

	//runs the contact form and sends it to ajax. receives and does something
	$('.contact.footer input[type=submit]').click(function(e){
		e.preventDefault();

		var name = $('.contact.footer input[name=name]').val();
		var email = $('.contact.footer input[name=email]').val();
		var message = $('.contact.footer textarea[name=email-message]').val();
		var answer = parseInt($('input[name=answer]').val());
		var user_answer = parseInt($('input[name=user_answer]').val());
		if(name.length == 0 || email.length == 0 || message.length == 0 || user_answer.length == 0 || user_answer + 3 != answer){
			var errors = '';
			if(name.length == 0){
				errors += 'Please provide a full name. <br>';
			}
			if(email.indexOf('@') == -1){
				errors += "Please enter a valid email address. <br>"
			}
			if(message.length == 0){
				errors += "Please give us some details. <br>"
			}
			if(user_answer.length == 0){
				errors += "Please answer the math question. (For Spam Purposes)"
			}
			if(user_answer + 3 != answer){
				errors += "Your answer in incorrect.";
			}
			$('.message').html(errors).addClass("error");

			//send an Analytics event for errors in the form
			ga('send', 'event', 'error', 'submit', 'contact-form');
		}
		else{
			$.post(ajaxUrl,{
				action: 'submit_contact_form',
				name: name,
				email: email,
				message: message,
				answer: answer
			}, function(resp){
				if(resp == true){
					$('.message').html('Your message has been sent.').addClass("success").removeClass("error");
					ga('send', 'event', 'form', 'submit contact-form', 'success');
				}
			}, "json");
		}
	})
	//var src = $('.user').css('background-image');
	//var url = src.match(/\((.*?)\)/)[1].replace(/('|")/g,'');

	// //hack to remove  g from the images.
	// $('.inner:has(img)').css('margin', 0); //remove the margin on the inners with an img (currently not available through css)

	var img = new Image();
	img.onload = function() {
		$('.three.first, .three.last').animate({opacity:1}, {
			duration: 2000,
			easing: 'easeInBack'},
			function(){

			}
		);
		$('.user').animate({opacity:1}, 2000, 'easeInBack',
			function(){
				$(this).children('.people-overlay').css('display', 'inline');
				$(this).children('.person-info').css('display', 'inline');
			}
		);
	}
	//img.src = url;
	if (img.complete) img.onload();

	$('.portfolio-item').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
		if (isInView && !$(this).attr('data-content-loaded')) {
			
			$(this).find('.lazy').trigger('lazy_trigger');

			//capture an Analytics event the first time a user scrolls to a portfolio item
			ga('send', 'event', 'navigation', 'scroll-enter', $(this).data('ga-shortname'));

		} else {
		// element has gone out of viewport
		}
	});

	$('.contact, .clients, .profiles').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
		if (isInView && !$(this).attr('ga-scroll')) {
			$(this).find('.lazy').trigger('lazy_trigger');
			// element is now visible in the viewport
			if (visiblePartY == 'top') {
				//console.log('top')
				//console.log($(this).attr('data-post-id'))
				othis = this

				$(this).attr('ga-scroll', true);
			}

			//capture an Analytics event the first time a user scrolls to the contact or clients panes
			ga('send', 'event', 'navigation', 'scroll-enter', $(this).attr('class'));
			
		} else {
		// element has gone out of viewport
		}
	});

	//begin counting the seconds the user is viewing the animation
	var viewing_animation = 0;
	var view_interval = setInterval(function () {
	  ++viewing_animation;
	}, 1000);
	$('.hero').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
		if (!isInView && !$(this).attr('ga-scroll')) {
			//capture an event when the user leaves the animation 'hero' pane
			ga('send', 'event', 'UX', 'view', 'hero animation', viewing_animation);

			//clear the second counter
			clearInterval(view_interval);
			$(this).attr('ga-scroll', true);
		}
	});

	$('.contact_us').click(function() {
		$('html, body').animate({
			scrollTop: $('.contact.footer').offset().top
		}, 750);

		//capture an Analytics event when the user clicks the contact nav button in the footer
		ga('send', 'event', 'navigation', 'click', 'footer contact-button');
	});
});//end jquery ready

if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {

}

else {
	/*
	//run the hero animation script from adobe edge
	jQuery(window).ready(function(){
		jQuery('.hero').addClass('loaded');
		runAdobe(AdobeEdge);
	});

	//function to run the adobe edge script
	function runAdobe(AdobeEdge) {
			AdobeEdge.loadComposition('wp-content/themes/pica/scripts/Pica_Animation', 'EDGE-22303902', {
			scaleToFit: "height",
			centerStage: "vertical",
			minW: "0",
			maxW: "undefined",
			width: "1352px",
			height: jQuery(window).height() + "px"
		}, {"dom":{}}, {"dom":{}});
	}
	*/
	jQuery(document).ready(function($) {
		$('.user').hoverIntent(
			function(){
				if($(window).width() > 750){
					$(this).children('.people-overlay').animate({
						height: '100%'
					},{
						duration:450,
						easing: 'easeOutCubic'
					},function(){

					})
					$(this).children('.person-info').animate({
						bottom: '5%'
					},{
						duration:450,
						easing: 'easeOutCubic'
					},function(){})
				}
			},
			function(){
				if($(window).width() > 750){
					$(this).children('.people-overlay').animate({
						height: '0%'
					},{
						duration:450,
						easing: 'easeOutCubic'
					},function(){

					});
					$(this).children('.person-info').animate({
						bottom: '-100px',
					},{
						duration:450,
						easing: 'easeOutCubic'
			},
			function(){

			});
		}});
	});
}



//functionfor fading in images.
function imgLoaded(img){
	var imgWrapper = img.parentNode;
	imgWrapper.className += imgWrapper.className ? ' loaded' : 'loaded';
	//runAdobe(AdobeEdge);
}

jQuery(function($) {
	$('img.lazy, div.lazy').lazyload();
});