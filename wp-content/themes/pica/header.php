<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <title><?php bloginfo('name'); ?></title>
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" type="text/css" media="screen" />
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
        <meta name="viewport" content="width=320, initial-scale=1">
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
		<link rel="icon" href="/favicon.ico" type="image/x-icon">
        <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
        <?php wp_head(); ?>

    <style>
        .edgeLoad-EDGE-22303902 { visibility:hidden; }
    </style>

    <?php /* GOOGLE ANALYTICS */ ?>
    <script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-4265805-59', 'auto');
	  ga('send', 'pageview');

	</script>

    </head>
    <body>
	  <div id="root">
	  	<header>
	  		<div class="container">
	  			<div class="inner-header">
		  			<div class="row">
		  				<div class="three columns">
							<div class="icon pica logo large">
								<?php if(is_front_page()) {
									?><a id="home"></a>
								<?php } else { ?>
									  <a id="home" href="/"></a>
								<? } ?>
							</div>
						</div>
						<div class="nine columns">
							<div class="menu">
								<nav>
									<ul>
										<?php if(is_front_page()) { ?>
											<li id="work">
											Work
											</li>
											<li id="clients">
												Clients
											</li>
											<li id="about">
												About
											</li>
											<li id="contact">
												Contact
											</li>
										<?php } else { ?>
											<a href="/#portfolio-item">
											Work
											</a>
											<a href="/#clients" id="clients">
												Clients
											</a>
											<a href="/#profiles">
												About
											</a>
											<a href="/#contact">
												Contact
											</a>
										<? } ?>
									</ul>
								</nav>

							</div>
						</div>
					</div>
				</div>
			</div>
	  	</header>
