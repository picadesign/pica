<?php get_header(); ?>
<?php /* setup the portfolio items */

$args = array(
	'post_type'   => 'portfolio-item',
	'post_status' => 'publish',
	'posts_per_page'	=>	-1,
	// //Custom Field Parameters
	'meta_key'       => '_featured-peice',
	'meta_value'     => 'true',
	'orderby'			=> 'menu_order',
	'order'				=> 'ASC'
);

$query = new WP_Query( $args );
// The Loop
if ( $query->have_posts() ) :
	while ( $query->have_posts() ) :
		$query->the_post(); ?>
		<div class="portfolio-display <?php echo $post->post_name ?>" style="overflow-y: scroll">
							<?php

							$post = get_post((int)$_POST['post_id']);
							setup_postdata($post);
							remove_filter('the_content', 'wpautop');
							the_content();
							add_filter('the_content', 'wpautop');

							?>
							<?php //remove_filter('the_content', 'wpautop'); ?>
							<?php //the_content(); ?>
							<?php //add_filter('the_content', 'wpautop'); ?>
							<?php /* CODE FOR PULLING TESTIMONIALS FROM THE DATABASE
							<div class="container test-wrapper">
								<div class="testimonial">
									<div class="content">
										"<?php echo get_post_meta( $post->ID, '_testimonial', true )['content']; ?>"
									</div>
									<div class="name">
										<?php echo get_post_meta( $post->ID, '_testimonial', true )['name']; ?>
									</div>
									<div class="creds">
										<?php echo get_post_meta( $post->ID, '_testimonial', true )['creds']; ?>
									</div>
								</div>
							</div>
							*/ ?>
							<div class="container prev_next">
								<div class="button blue more_work">More Work</div>
							</div>
							<div class="close <?php echo $post->post_name; ?>"></div>
						</div>
<?php endwhile;
endif;
wp_reset_postdata();
 ?>

<?php if ( have_posts() ) :
			while ( have_posts() ) : the_post(); ?>
<div class="sections-wrapper" id="fullpage">
	<div class="full hero home section">
		<div id="Stage" class="EDGE-22303902"></div>
		<div class="logo-long icon"></div>
		<div id="mobile_animation">
			<div id="mobile_banner">THE IDEA IS</div>
			<div id="brand" style="opacity: 0">BRAND
				<span style="position: absolute;">SMART</span>
				<span style="opacity: 0; position: absolute;">SIMPLE</span>
				<span style="opacity: 0">STRONG.</span>
			</div>
		</div>
	</div>
	<div class="full section dark-grey mission-statement">
		<div class="container">
			<div class="statement vert-center">
				<?php echo $post->post_content; ?>
			</div>
		</div>
	</div>
	<?php
			endwhile;
		endif;
	?>
	<?php
			/**
			 * The WordPress Query class.
			 * @link http://codex.wordpress.org/Function_Reference/WP_Query
			 *
			 */
			$args = array(
				'post_type'   => 'portfolio-item',
				'post_status' => 'publish',
				'posts_per_page'	=>	-1,
				// //Custom Field Parameters
				'meta_key'       => '_featured-peice',
				'meta_value'     => 'true',
				'orderby'			=> 'menu_order',
				'order'				=> 'ASC'
			);

		$query = new WP_Query( $args );
		// The Loop
		if ( $query->have_posts() ) :
			while ( $query->have_posts() ) :
				$query->the_post(); ?>
				<div class="full section <?php echo $post->post_type ?>" style="background-color:<?php echo get_post_meta($post->ID, '_homepage-color', true) ?>;" data-post-id="<?php echo $post->ID ?>" data-ga-shortname="<?php echo $post->post_name; ?>">
					<div class="container">
						<div class="row">
							<div class="four columns alpha <?php  echo (get_post_meta($post->ID, '_image-position', true) == 'left' ?'right' : '');?>">
								<h3><?php the_title(); ?></h3>
								<p><?php the_excerpt(); ?></p>
								<div class="button white view-project" data-project-name="<?php echo $post->post_name ?>" data-post-id="<?php echo $post->ID; ?>">View Work</div>
							</div>
							<div class="one columns <?php  echo (get_post_meta($post->ID, '_image-position', true) == 'left' ?'right' : '');?>"></div>
							<div class="seven columns omega <?php  echo (get_post_meta($post->ID, '_image-position', true) == 'left' ?'left' : '');?>" style="float:left;">
								<span>
									<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full'); ?>

									<img class="lazy" width="<?php echo $image[1] ?>" height="<?php echo $image[2] ?>" data-src="<?php echo $image[0] ?>" data-original="<?php echo $image[0] ?>">
									<?php //the_post_thumbnail(); ?>
								</span>
							</div>
						</div>
					</div>
				</div>
			<?php endwhile;
		endif;
		/* Restore original Post Data */
		wp_reset_postdata();
	?>

	<div class="full section light-grey clients">
		<div class="container">
			<div class="row">
				<?php
					/**
					 * The WordPress Query class.
					 * @link http://codex.wordpress.org/Function_Reference/WP_Query
					 *
					 */
					$args = array(
						'post_parent'  => 22,
						'post_type'   => 'attachment',
						'post_status' => 'inherit',
						'posts_per_page' => -1,
						'orderby' => 'menu_order',
						'order' => 'ASC'
					);

					$clients = new WP_Query( $args );
					if ( $clients->have_posts() ) :
						$count = 0;
						while ( $clients->have_posts() ) :
							$clients->the_post(); ?>
								<div class="three columns">

									<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full'); ?>

									<img class="lazy" width="<?php echo $image[1] ?>" height="<?php echo $image[2] ?>" data-src="<?php echo $image[0] ?>" data-original="<?php echo $image[0] ?>">
									<?php //the_post_thumbnail(); ?>
								</div>
								<?php $count++; ?>
								<?php if($count%4 == 0 && $count != count($clients->posts)): ?>
									</div>
									<div class="row">
								<?php endif; ?>
						<?php endwhile; ?>
					<?php endif; wp_reset_postdata(); ?>
			</div>
		</div>
	</div>

	<div class="full section profiles">
		<div class="section-head pica-blue">
			<div class="container">
				<div class="row"><h1>AT THE END OF THE DAY, IT'S ABOUT WORKING TOGETHER</h1></div>
			</div>
		</div>
		<div class="section-body">
			<div class="three columns pica-blue first">
				<div class="vert-center">
					<h1>PICA PEOPLE</h1>
					<div class="r-arrow white"></div>
				</div>
			</div>
			<?php
				$args = array(
					'blog_id'      => $GLOBALS['blog_id'],
					'role'         => 'administrator',
					'meta_key'     => '_user_menu_order',
					'exclude'      => array(1),
					'orderby'      => 'meta_value',
					'order'        => 'ASC',
					'number'       => '6', /* dont allow more than 6 for layout reasons */
				 );

				$blogusers = get_users( $args );

				$count = 1;
				foreach ( $blogusers as $user ) : ?>
					<div class="three columns user lazy" data-original="<?php echo str_replace('-512x512', '', get_avtr_url(get_avatar($user->ID, 800))) ?>">
						<div class="people-overlay"></div>
						<div class="person-info">
							<div class="name"><?php echo $user->first_name. ' '. $user->last_name ?></div>
							<div class="position"><?php echo get_user_meta( $user->ID, '_position_title', true ); ?></div>
							<div class="social-networks">
								<?php if(get_user_meta( $user->ID, '_user_facebook', true )):?>
									<div class="icon facebook white"><a href="<?php echo antispambot(get_user_meta( $user->ID, '_user_facebook', true )); ?>" target="_blank"></a></div>
								<?php endif; ?>
								<?php if(get_user_meta( $user->ID, '_user_twitter', true )):?>
									<div class="icon twitter white"><a href="<?php echo antispambot(get_user_meta( $user->ID, '_user_twitter', true )); ?>" target="_blank"></a></div>
								<?php endif; ?>
								<?php if(get_user_meta( $user->ID, '_user_instagram', true )):?>
									<div class="icon instagram white"><a href="<?php echo antispambot(get_user_meta( $user->ID, '_user_instagram', true )); ?>" target="_blank"></a></div>
								<?php endif; ?>
								<?php if(get_user_meta( $user->ID, '_user_linkedin', true )):?>
									<div class="icon linkedin white"><a href="<?php echo antispambot(get_user_meta( $user->ID, '_user_linkedin', true )); ?>" target="_blank"></a></div>
								<?php endif; ?>
								<?php if ($user->user_email): ?>
									<div class="icon mail white"><a href="mailto:<?php echo antispambot( $user->user_email, true ); ?>"></a></div>
								<?php endif ?>
							</div>
						</div>
					</div>
					<?php $count++;
					if($count%4 == 0):
						//echo '<div class="three columns"></div><div class="three columns"></div>';
					endif;
				endforeach;
			?>
			<div class="three columns pica-blue last">
				<div class="vert-center">
					<h1>Current Positions Available:</h1>
					<h1><a href="mailto:<?php echo antispambot('jobs@pica.is', true); ?>?Subject=Job%20Inquiry:%20Traffic%20Manager">Traffic Manager</a></h1>
				</div>
			</div>
			</div>
		</div>
		<div class="full section white contact footer">
			<div class="container">
				<div class="top row">
					<h3>Think It Through With Us</h3>
					<span class="contact_subheading">We're easy to talk to.</span>
				</div>
				<div class="row middle">
					<div class="three columns information">
						<h2>Contact</h2>

						<?php
							$street = (get_option('qs_contact_street'));
							$street = str_replace(" br ", '<br>', $street);
							echo $street;
						?>
						<br>
						<?php echo (get_option('qs_contact_city') . ', ' . get_option('qs_contact_state') . ' ' . get_option('qs_contact_zip')) ?>
						<br>
						<strong>phone: </strong><?php echo antispambot(get_option('qs_contact_phone')); ?>
						<br>
						<strong>fax: </strong><?php echo antispambot(get_option('qs_contact_fax')); ?>
						<br>
						<strong>email: </strong><a href="mailto:<?php echo antispambot(get_option('qs_contact_email'), true ); ?>"><?php echo antispambot(get_option('qs_contact_email'), false ); ?></a>
					</div>
					<div class="six columns">
						<h2>Connect</h2>
						<form>
							<input type="text" name="name" placeholder="Name">
							<input type="email" name="email" placeholder="E-mail">
							<textarea name="email-message" placeholder="How can we help?" rows="10"></textarea>
							<?php $answer = rand(1,9) ?>
							<input type="hidden" name="answer" value="<?php echo $answer ?>">
							<?php echo antispambot("Three") ?> + <input type="text" name="user_answer"> = <?php echo $answer ?> <input type="submit" placeholder="Submit">
							</form>
						<div class="message"></div>
					</div>
					<div class="three columns instagram">
						<h2>Instagram</h2>
						<?php echo do_shortcode("[instagram-feed]" ); ?>
						<p><a href="<?php echo get_option('qs_contact_custom_instagram'); ?>" target="_blank">view on instagram</a></p>
						<!--<div class="six columns alpha">
							<img src="http://placehold.it/300" width="100%" height="auto">
						</div>
						<div class="six columns">
							<img src="http://placehold.it/300" width="100%" height="auto">
						</div>
						<div class="twelve columns alpha">
							<img src="http://placehold.it/300" width="100%" height="auto">
						</div>-->
					</div>
				</div>
			</div>
			<div class="bottom-footer">
				<div class="container">
					<div class="row">
						<div class="copyright">
							<span class="contact_us">Contact</span> // <a href="<?php echo get_the_permalink(399); ?>">Terms and Conditions</a> // © <?php echo date("Y") ?> Pica
						</div>
						<div class="small_copyright">
							<span class="contact_us">Contact</span> // <a href="<?php echo get_the_permalink(399); ?>">Terms and Conditions</a> // © <?php echo date("Y") ?> Pica
						</div>
						<div class="social-links">
							<div class="icon facebook"><a href="<?php echo get_option('qs_contact_facebook'); ?>" target="_blank"></a></div>
							<div class="icon twitter"><a href="<?php echo get_option('qs_contact_twitter'); ?>" target="_blank"></a></div>
							<div class="icon instagram"><a href="<?php echo get_option('qs_contact_custom_instagram'); ?>" target="_blank"></a></div>
							<div class="icon linkedin"><a href="<?php echo get_option('qs_contact_linkedin'); ?>" target="_blank"></a></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> <?php //end fullpage ?>

<?php get_footer(); ?>