
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `pica_wp_usermeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pica_wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `pica_wp_usermeta` WRITE;
/*!40000 ALTER TABLE `pica_wp_usermeta` DISABLE KEYS */;
INSERT INTO `pica_wp_usermeta` VALUES (1,1,'nickname','picadesign'),(2,1,'first_name',''),(3,1,'last_name',''),(4,1,'description',''),(5,1,'rich_editing','true'),(6,1,'comment_shortcuts','false'),(7,1,'admin_color','fresh'),(8,1,'use_ssl','0'),(9,1,'show_admin_bar_front','true'),(10,1,'pica_wp_capabilities','a:1:{s:13:\"administrator\";b:1;}'),(11,1,'pica_wp_user_level','10'),(12,1,'dismissed_wp_pointers','vc_pointers_backend_editor'),(13,1,'show_welcome_panel','1'),(14,1,'session_tokens','a:3:{s:64:\"6b569b5f4dc67e1e833347b0a1cae68c2c7fd6fa6ce73ec4b37d3ff8e16f9fd5\";a:4:{s:10:\"expiration\";i:1459529162;s:2:\"ip\";s:15:\"216.195.184.229\";s:2:\"ua\";s:120:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36\";s:5:\"login\";i:1459356362;}s:64:\"5f315d0fc033afdb573b1bd4dd2aae2360a4e620c74fd6d2b6a76f664e0711ce\";a:4:{s:10:\"expiration\";i:1459695277;s:2:\"ip\";s:15:\"216.195.184.229\";s:2:\"ua\";s:120:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36\";s:5:\"login\";i:1459522477;}s:64:\"5479bd387ef43b6b90baeeae34078c05fc539900e69faaf69221176ac3a4ef76\";a:4:{s:10:\"expiration\";i:1459695649;s:2:\"ip\";s:15:\"216.195.184.229\";s:2:\"ua\";s:120:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36\";s:5:\"login\";i:1459522849;}}'),(15,1,'pica_wp_dashboard_quick_press_last_post_id','15883'),(16,1,'pica_wp_user-settings','libraryContent=browse&editor=tinymce&edit_element_vcUIPanelWidth=650&edit_element_vcUIPanelLeft=710px&edit_element_vcUIPanelTop=74px'),(17,1,'pica_wp_user-settings-time','1457715962'),(18,1,'meta-box-order_portfolio_page','a:3:{s:4:\"side\";s:80:\"submitdiv,portfolio_categorydiv,tagsdiv-portfolio_tag,pageparentdiv,postimagediv\";s:6:\"normal\";s:78:\"wpb_visual_composer,postexcerpt,commentstatusdiv,commentsdiv,slugdiv,authordiv\";s:8:\"advanced\";s:385:\"qodef-meta-box-portfolio_images,qodef-meta-box-portfolio_images_videos2,qodef-meta-box-portfolio_properties,qodef-meta-box-portfolio_general,qodef-meta-box-porfolio_header,qodef-meta-box-porfolio_left_menu,qodef-meta-box-porfolio_title,qodef-meta-box-portfolio_title_animations,qodef-meta-box-portfolio_content_bottom_page,qodef-meta-box-portfolio_side_bar,qodef-meta-box-portfolio_seo\";}'),(19,1,'screen_layout_portfolio_page','2'),(20,1,'nav_menu_recently_edited','75'),(21,1,'managenav-menuscolumnshidden','a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),(22,1,'metaboxhidden_nav-menus','a:9:{i:0;s:28:\"add-post-type-portfolio_page\";i:1;s:12:\"add-post_tag\";i:2;s:15:\"add-post_format\";i:3;s:22:\"add-portfolio_category\";i:4;s:17:\"add-portfolio_tag\";i:5;s:25:\"add-testimonials_category\";i:6;s:19:\"add-slides_category\";i:7;s:22:\"add-carousels_category\";i:8;s:28:\"add-masonry_gallery_category\";}'),(23,1,'layerslider_help_wp_pointer','1'),(24,1,'closedpostboxes_portfolio_page','a:1:{i:0;s:11:\"postexcerpt\";}'),(25,1,'metaboxhidden_portfolio_page','a:2:{i:0;s:7:\"slugdiv\";i:1;s:33:\"qodef-meta-box-porfolio_left_menu\";}'),(26,1,'pica_wp_media_library_mode','grid'),(27,1,'wpseo_ignore_tour','1'),(28,1,'wpseo_seen_about_version','3.1.1'),(29,1,'closedpostboxes_slides','a:0:{}'),(30,1,'metaboxhidden_slides','a:2:{i:0;s:7:\"slugdiv\";i:1;s:36:\"qodef-meta-box-slides_video_settings\";}'),(31,1,'tinypng_admin_notice_dismissals','a:1:{s:13:\"limit-reached\";b:1;}');
/*!40000 ALTER TABLE `pica_wp_usermeta` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

