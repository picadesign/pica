
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `pica_wp_terms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pica_wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `pica_wp_terms` WRITE;
/*!40000 ALTER TABLE `pica_wp_terms` DISABLE KEYS */;
INSERT INTO `pica_wp_terms` VALUES (1,'Uncategorized','uncategorized',0),(2,'Art','art',0),(3,'Business','business',0),(4,'Design','design',0),(5,'Music','music',0),(6,'Photography','photography',0),(7,'Sport','sport',0),(8,'Analysis','analysis',0),(9,'Art','art',0),(10,'Articles','articles',0),(11,'Audio','audio',0),(12,'Articles','articles-2',0),(13,'Business','business',0),(14,'Culture','culture',0),(16,'Development','development',0),(18,'Ecology','ecology',0),(19,'Events','events',0),(21,'Information','information',0),(22,'Inspiration','inspiration',0),(25,'Nature','nature',0),(27,'Opportunities','opportunities',0),(31,'Science','science',0),(33,'Sport','sport',0),(36,'Sport','sport-2',0),(37,'Stress','stress',0),(39,'Stress','stress-2',0),(40,'Swimming','swimming',0),(44,'Trends','trends',0),(47,'Trends','trends-2',0),(49,'Trends','trends-3',0),(50,'Video','video',0),(54,'Web Design','web-design',0),(56,'Web Design','web-design-2',0),(57,'Arts','arts',0),(63,'Business','business',0),(66,'Home Slider','home-slider',0),(70,'Home Testimonials','home-testimonials',0),(71,'Photography','photography',0),(75,'main-menu','main-menu',0),(78,'main-menu','main-menu-2',0),(80,'main-menu','main-menu-3',0),(81,'Gallery','post-format-gallery',0),(82,'Audio','post-format-audio',0),(83,'Link','post-format-link',0),(84,'Quote','post-format-quote',0),(85,'Video','post-format-video',0),(86,'Healthcare','healthcare',0),(87,'Technology','technology',0),(88,'Music','music',0),(89,'Tourism','tourism',0),(90,'Museum','museum',0),(91,'Local Economy','local-economy',0),(92,'Education','education',0);
/*!40000 ALTER TABLE `pica_wp_terms` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

