
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `pica_wp_commentmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pica_wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `pica_wp_commentmeta` WRITE;
/*!40000 ALTER TABLE `pica_wp_commentmeta` DISABLE KEYS */;
INSERT INTO `pica_wp_commentmeta` VALUES (1,26,'_wp_trash_meta_status','0'),(2,26,'_wp_trash_meta_time','1458166416'),(3,25,'_wp_trash_meta_status','0'),(4,25,'_wp_trash_meta_time','1458166416'),(5,40,'_wp_trash_meta_status','0'),(6,40,'_wp_trash_meta_time','1458166416'),(7,39,'_wp_trash_meta_status','0'),(8,39,'_wp_trash_meta_time','1458166416'),(9,38,'_wp_trash_meta_status','0'),(10,38,'_wp_trash_meta_time','1458166416'),(11,37,'_wp_trash_meta_status','0'),(12,37,'_wp_trash_meta_time','1458166416'),(13,36,'_wp_trash_meta_status','0'),(14,36,'_wp_trash_meta_time','1458166416'),(15,35,'_wp_trash_meta_status','0'),(16,35,'_wp_trash_meta_time','1458166416'),(17,34,'_wp_trash_meta_status','0'),(18,34,'_wp_trash_meta_time','1458166416'),(19,16,'_wp_trash_meta_status','0'),(20,16,'_wp_trash_meta_time','1458166416'),(21,27,'_wp_trash_meta_status','0'),(22,27,'_wp_trash_meta_time','1458166416'),(23,24,'_wp_trash_meta_status','0'),(24,24,'_wp_trash_meta_time','1458166416'),(25,33,'_wp_trash_meta_status','0'),(26,33,'_wp_trash_meta_time','1458166416'),(27,32,'_wp_trash_meta_status','0'),(28,32,'_wp_trash_meta_time','1458166416'),(29,31,'_wp_trash_meta_status','0'),(30,31,'_wp_trash_meta_time','1458166416'),(31,30,'_wp_trash_meta_status','0'),(32,30,'_wp_trash_meta_time','1458166416'),(33,29,'_wp_trash_meta_status','0'),(34,29,'_wp_trash_meta_time','1458166416'),(35,21,'_wp_trash_meta_status','0'),(36,21,'_wp_trash_meta_time','1458166416'),(37,20,'_wp_trash_meta_status','0'),(38,20,'_wp_trash_meta_time','1458166416'),(39,15,'_wp_trash_meta_status','0'),(40,15,'_wp_trash_meta_time','1458166416'),(41,14,'_wp_trash_meta_status','0'),(42,14,'_wp_trash_meta_time','1458166430'),(43,13,'_wp_trash_meta_status','0'),(44,13,'_wp_trash_meta_time','1458166430'),(45,12,'_wp_trash_meta_status','0'),(46,12,'_wp_trash_meta_time','1458166430'),(47,11,'_wp_trash_meta_status','0'),(48,11,'_wp_trash_meta_time','1458166430'),(49,17,'_wp_trash_meta_status','0'),(50,17,'_wp_trash_meta_time','1458166430'),(51,19,'_wp_trash_meta_status','0'),(52,19,'_wp_trash_meta_time','1458166430'),(53,10,'_wp_trash_meta_status','0'),(54,10,'_wp_trash_meta_time','1458166430'),(55,9,'_wp_trash_meta_status','0'),(56,9,'_wp_trash_meta_time','1458166430'),(57,28,'_wp_trash_meta_status','0'),(58,28,'_wp_trash_meta_time','1458166430'),(59,8,'_wp_trash_meta_status','0'),(60,8,'_wp_trash_meta_time','1458166430'),(61,7,'_wp_trash_meta_status','0'),(62,7,'_wp_trash_meta_time','1458166430'),(63,6,'_wp_trash_meta_status','0'),(64,6,'_wp_trash_meta_time','1458166430'),(65,5,'_wp_trash_meta_status','0'),(66,5,'_wp_trash_meta_time','1458166430'),(67,23,'_wp_trash_meta_status','0'),(68,23,'_wp_trash_meta_time','1458166430'),(69,4,'_wp_trash_meta_status','0'),(70,4,'_wp_trash_meta_time','1458166430'),(71,22,'_wp_trash_meta_status','0'),(72,22,'_wp_trash_meta_time','1458166430');
/*!40000 ALTER TABLE `pica_wp_commentmeta` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

