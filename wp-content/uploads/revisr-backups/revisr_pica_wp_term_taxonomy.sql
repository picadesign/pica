
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `pica_wp_term_taxonomy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pica_wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `pica_wp_term_taxonomy` WRITE;
/*!40000 ALTER TABLE `pica_wp_term_taxonomy` DISABLE KEYS */;
INSERT INTO `pica_wp_term_taxonomy` VALUES (1,1,'category','',0,1),(2,2,'category','',0,4),(3,3,'category','',0,2),(4,4,'category','',0,2),(5,5,'category','',0,2),(6,6,'category','',0,1),(7,7,'category','',0,2),(8,8,'post_tag','',0,3),(9,9,'post_tag','',0,6),(10,10,'post_tag','',0,9),(11,11,'post_tag','',0,2),(12,12,'post_tag','',0,0),(13,13,'post_tag','',0,2),(14,14,'post_tag','',0,6),(16,16,'post_tag','',0,1),(17,18,'post_tag','',0,3),(19,19,'post_tag','',0,3),(21,21,'post_tag','',0,5),(22,22,'post_tag','',0,2),(24,25,'post_tag','',0,2),(26,27,'post_tag','',0,1),(31,31,'post_tag','',0,2),(33,33,'post_tag','',0,0),(36,36,'post_tag','',0,0),(37,37,'post_tag','',0,0),(40,40,'post_tag','',0,0),(41,39,'post_tag','',0,0),(45,44,'post_tag','',0,1),(47,47,'post_tag','',0,0),(49,49,'post_tag','',0,0),(50,50,'post_tag','',0,2),(54,54,'post_tag','',0,0),(56,56,'post_tag','',0,0),(57,57,'portfolio_category','',0,1),(63,63,'portfolio_category','',0,0),(66,66,'slides_category','',0,3),(70,70,'testimonials_category','',0,6),(73,71,'portfolio_category','',0,0),(75,75,'nav_menu','',0,6),(78,78,'nav_menu','',0,0),(79,80,'nav_menu','',0,0),(81,81,'post_format','',0,2),(82,82,'post_format','',0,2),(83,83,'post_format','',0,2),(84,84,'post_format','',0,2),(85,85,'post_format','',0,2),(86,86,'portfolio_category','',0,2),(87,87,'portfolio_category','',0,1),(88,88,'portfolio_category','',0,0),(89,89,'portfolio_category','',0,0),(90,90,'portfolio_category','',0,1),(91,91,'portfolio_category','',0,1),(92,92,'portfolio_category','',0,0);
/*!40000 ALTER TABLE `pica_wp_term_taxonomy` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

