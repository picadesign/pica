
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `pica_wp_wfIssues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pica_wp_wfIssues` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `time` int(10) unsigned NOT NULL,
  `status` varchar(10) NOT NULL,
  `type` varchar(20) NOT NULL,
  `severity` tinyint(3) unsigned NOT NULL,
  `ignoreP` char(32) NOT NULL,
  `ignoreC` char(32) NOT NULL,
  `shortMsg` varchar(255) NOT NULL,
  `longMsg` text,
  `data` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `pica_wp_wfIssues` WRITE;
/*!40000 ALTER TABLE `pica_wp_wfIssues` DISABLE KEYS */;
INSERT INTO `pica_wp_wfIssues` VALUES (60,1459509978,'new','wfPluginUpgrade',1,'a68d53426ebeecc285de7ad51f0abac5','a68d53426ebeecc285de7ad51f0abac5','The Plugin \"Compress JPEG &amp; PNG images\" needs an upgrade.','You need to upgrade \"Compress JPEG &amp; PNG images\" to the newest version to ensure you have any security fixes the developer has released.','a:13:{s:4:\"Name\";s:30:\"Compress JPEG &amp; PNG images\";s:9:\"PluginURI\";s:0:\"\";s:7:\"Version\";s:5:\"1.7.0\";s:11:\"Description\";s:143:\"Speed up your website. Optimize your JPEG and PNG images automatically with TinyPNG. <cite>By <a href=\"https://tinypng.com\">TinyPNG</a>.</cite>\";s:6:\"Author\";s:41:\"<a href=\"https://tinypng.com\">TinyPNG</a>\";s:9:\"AuthorURI\";s:19:\"https://tinypng.com\";s:10:\"TextDomain\";s:20:\"tiny-compress-images\";s:10:\"DomainPath\";s:0:\"\";s:7:\"Network\";b:0;s:5:\"Title\";s:30:\"Compress JPEG &amp; PNG images\";s:10:\"AuthorName\";s:7:\"TinyPNG\";s:10:\"pluginFile\";s:90:\"/home/picade5/public_html/wp-content/plugins/tiny-compress-images/tiny-compress-images.php\";s:10:\"newVersion\";s:5:\"1.7.1\";}'),(58,1459509975,'new','file',1,'add1f605ca8ea55fdf8190f20a05a3c0','f24c70bd5e5e8a386d3b82fb453a867d','This file may contain malicious executable code: /home/picade5/public_html/old_new_pica/wp-admin/includes/class-pclzip.php','This file is a PHP executable file and contains the word \'eval\' (without quotes) and the word \'unpack(\' (without quotes). The eval() function along with an encoding function like the one mentioned are commonly used by hackers to hide their code. If you know about this file you can choose to ignore it to exclude it from future scans.','a:4:{s:4:\"file\";s:47:\"old_new_pica/wp-admin/includes/class-pclzip.php\";s:7:\"canDiff\";b:0;s:6:\"canFix\";b:0;s:9:\"canDelete\";b:1;}'),(59,1459509975,'new','file',1,'ffb5edda9019d5acf15eb52e207ca3ca','f36f22a7f9559b4027c22c21d28be40a','This file may contain malicious executable code: /home/picade5/public_html/wp-content/plugins/js_composer/assets/lib/php.default/php.default.min.js','This file is a PHP executable file and contains the word \'eval\' (without quotes) and the word \'base64_decode(\' (without quotes). The eval() function along with an encoding function like the one mentioned are commonly used by hackers to hide their code. If you know about this file you can choose to ignore it to exclude it from future scans.','a:4:{s:4:\"file\";s:72:\"wp-content/plugins/js_composer/assets/lib/php.default/php.default.min.js\";s:7:\"canDiff\";b:0;s:6:\"canFix\";b:0;s:9:\"canDelete\";b:1;}');
/*!40000 ALTER TABLE `pica_wp_wfIssues` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

